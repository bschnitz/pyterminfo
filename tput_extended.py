#!/usr/bin/env python3

# pyterminfo can be used to communicate with the terminal using terminal
# capatibilities similar to the tput terminal command (see man tput)
#
# this file contains an extended version of the Tput class, which offers
# additional abstraction (for e.g. color management)
#
# Copyright (C) 2016  Benjamin Schnitzler
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from .tput import Tput

class TputExtended(Tput):
  colors = {
    'black'      : 0,
    'red'        : 1,
    'green'      : 2,
    'yellow'     : 3,
    'blue'       : 4,
    'magenta'    : 5,
    'cyan'       : 6,
    'white'      : 7
  }
  def __init__(self):
    self.attrs = set([ 'fg', 'bg', 'hi' ])
    self.init()
    super().__init__()

  def init(self):
    self.chain = []
    return self

  def __getattr__(self, attr):
    if attr in TputExtended.colors:
      return self.color(attr)
    elif attr in self.attrs:
      self.chain.append(attr)
    else:
      return super().__getattr__(attr)
    return self

  def color(self, color):
    colormod = 8 if 'hi' in self.chain else 0
    if type(color) is str:
      color = TputExtended.colors[color]
    color += colormod
    if 'fg' in self.chain: self.setaf(color)
    else:                  self.setab(color)
    self.chain = []
    return self

  def end(self, *args, **kwargs):
    code = self.code
    self.set().sgr0.put(*args, **kwargs)
    self.code = code
    return self
