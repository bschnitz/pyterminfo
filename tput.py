#!/usr/bin/env python3

# pyterminfo can be used to communicate with the terminal using terminal
# capatibilities similar to the tput terminal command (see man tput)
#
# this file contains the basic Tput class
#
# Copyright (C) 2016  Benjamin Schnitzler
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import curses

class UnknownTerminfoCapatibility(Exception):
  def __init__(self, cap):
    super().__init__("unknown terminfo capability '"+cap+"'")

class Tput:
  def __init__(self):
    curses.setupterm()
    self.set()

  def set(self, *args):
    self.next = b""
    if len(args) > 0:
      self.code = args[0]
    else:
      self.code = ""
    return self

  def __getattr__(self, attr):
    self.code += self.next.decode(sys.stdout.encoding)
    self.next = curses.tigetstr(attr)
    if self.next == None:
      raise UnknownTerminfoCapatibility(attr)
    return self

  def get(self, cap):
    rval = curses.tigetflag(cap)
    if type(rval) is bool:
      return rval
    elif rval == 0:
      raise UnknownTerminfoCapatibility(cap)

    rval = curses.tigetnum(cap)
    if rval >= 0:
      return rval
    elif rval == -1:
      raise UnknownTerminfoCapatibility(cap)

    rval = curses.tigetstr(cap)
    if rval == None:
      raise UnknownTerminfoCapatibility(cap)

    return rval

  def __call__(self, *args):
    if self.next:
      if args:
        self.code += curses.tparm(self.next, *args).decode(sys.stdout.encoding)
      else:
        self.code += self.next.decode(sys.stdout.encoding)
      self.next = b""
      return self

  def __str__(self):
    self.code += self.next.decode(sys.stdout.encoding)
    self.next = b""
    return self.code

  def put(self, *args, **kwargs):
    print( self, end="" )
    kwargs['end'] = kwargs['end'] if 'end' in kwargs else ""
    print( *args, **kwargs )
    return self
